﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class NaturalArmor : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public NaturalArmor(int lowDef, int highDef)
        {
            name = "Natural Armor";
            IsNatural = true;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(lowDef, highDef);
            slot = InventorySlotId.CHESTPIECE;
        }
    }
}
