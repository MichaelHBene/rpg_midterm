﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Gambeson : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public Gambeson()
        {
            name = "Gambeson";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(0, 3);
            slot = InventorySlotId.CHESTPIECE;
        }
    }
}
