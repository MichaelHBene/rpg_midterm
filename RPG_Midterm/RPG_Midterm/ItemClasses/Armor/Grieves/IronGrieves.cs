﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class IronGrieves : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public IronGrieves()
        {
            name = "Iron Grieves";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(2, 6);
            slot = InventorySlotId.GRIEVES;
        }
    }
}
