﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class BronzeGrieves: Item, IArmor
    { 
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public BronzeGrieves()
        {
            name = "Bronze Grieves";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(0, 4);
            slot = InventorySlotId.GRIEVES;
        }
    }
}
