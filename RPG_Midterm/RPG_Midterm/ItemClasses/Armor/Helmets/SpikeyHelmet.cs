﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class SpikeyHelmet : Item, IArmor
    {
        public int DefenseValue { get; }
        public int AttackValue { get; }
        public bool IsNatural { get; }


        public SpikeyHelmet()
        {
            name = "Spikey Helmet";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(3, 7);
            AttackValue = randAttack.Next(4, 11);
            slot = InventorySlotId.HELMET;
        }
    }
}
