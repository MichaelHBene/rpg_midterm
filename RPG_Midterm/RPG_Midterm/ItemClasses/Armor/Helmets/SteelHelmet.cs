﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class SteelHelmet : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }


        public SteelHelmet()
        {
            name = "Steel Helmet";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(4, 8);
            slot = InventorySlotId.HELMET;
        }
    }
}
