﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class BronzeHelmet : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public BronzeHelmet()
        {
            name = "Bronze Helmet";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(0, 4);
            slot = InventorySlotId.HELMET;
        }
    }
}
