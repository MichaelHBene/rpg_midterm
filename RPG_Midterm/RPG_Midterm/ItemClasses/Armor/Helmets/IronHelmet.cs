﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class IronHelmet : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public IronHelmet()
        {
            name = "Iron Helmet";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(2, 6);
            slot = InventorySlotId.HELMET;
        }
    }
}
