﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class SteelVambraces : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public SteelVambraces()
        {
            name = "Steel Vambraces";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(4, 8);
            slot = InventorySlotId.VAMBRACES;
        }
    }
}
