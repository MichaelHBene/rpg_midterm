﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class IronVambraces : Item, IArmor
    {
        public int DefenseValue { get; }
        public bool IsNatural { get; }

        public IronVambraces()
        {
            name = "Iron Vambraces";
            IsNatural = false;
            Random randAttack = new Random();
            DefenseValue = randAttack.Next(2, 6);
            slot = InventorySlotId.VAMBRACES;
        }
    }
}
