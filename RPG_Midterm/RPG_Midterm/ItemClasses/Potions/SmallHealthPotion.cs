﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class SmallHealthPotion : Item, IPotion
    {
        public int HealValue { get; }

        public SmallHealthPotion()
        {
            name = "Small Health Potion";
            Random randHeal = new Random();
            HealValue = randHeal.Next(10, 16);
            slot = InventorySlotId.POTION1;
        }
    }
}
