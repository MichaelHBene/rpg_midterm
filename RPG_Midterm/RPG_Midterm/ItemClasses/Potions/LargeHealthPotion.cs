﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class LargeHealthPotion : Item, IPotion
    {
        public int HealValue { get; }

        public LargeHealthPotion()
        {
            name = "Large Health Potion";
            Random randHeal = new Random();
            HealValue = randHeal.Next(20, 25);
            slot = InventorySlotId.POTION1;
        }
    }
}
