﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class NaturalWeapon : Item, IWeapon
    {
        public int AttackValue { get; }
        public bool IsNatural { get; }


        public NaturalWeapon(int lowAtk, int highAtk)
        {
            name = "Natural Weapon";
            IsNatural = true;
            Random randAttack = new Random();
            AttackValue = randAttack.Next(lowAtk, highAtk);
            slot = InventorySlotId.WEAPON;
        }
    }
}
