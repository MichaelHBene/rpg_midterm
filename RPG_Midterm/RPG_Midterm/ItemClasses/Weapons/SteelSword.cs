﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class SteelSword : Item, IWeapon
    {
        public int AttackValue { get; }
        public bool IsNatural { get; }

        public SteelSword()
        {
            name = "Steel Sword";
            IsNatural = false;
            Random randAttack = new Random();
            AttackValue = randAttack.Next(12, 19);
            slot = InventorySlotId.WEAPON;

        }
    }
}
