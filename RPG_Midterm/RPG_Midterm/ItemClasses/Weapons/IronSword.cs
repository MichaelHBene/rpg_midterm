﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class IronSword : Item, IWeapon
    {
        public int AttackValue { get; }
        public bool IsNatural { get; }

        public IronSword()
        {
            name = "Iron Sword";
            IsNatural = false;
            Random randAttack = new Random();
            AttackValue = randAttack.Next(8, 15);
            slot = InventorySlotId.WEAPON;

        }
    }
}
