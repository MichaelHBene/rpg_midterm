﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class Buckler : Item, IWeapon
    {
        public int AttackValue { get; }
        public int DefenseValue { get; }
        public bool IsNatural { get; }


        public Buckler()
        {
            name = "Buckler";
            IsNatural = false;
            Random randAttack = new Random();
            AttackValue = randAttack.Next(4, 11);
            DefenseValue = randAttack.Next(3, 7);
            slot = InventorySlotId.WEAPON;

        }
    }
}
