﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class BronzeSword : Item, IWeapon
    {
        public int AttackValue { get;}
        public bool IsNatural { get; }

        public BronzeSword()
        {
            name = "Bronze Sword";
            IsNatural = false;
            Random randAttack = new Random();
            AttackValue = randAttack.Next(4, 11);
            slot = InventorySlotId.WEAPON;

        }
    }
}
