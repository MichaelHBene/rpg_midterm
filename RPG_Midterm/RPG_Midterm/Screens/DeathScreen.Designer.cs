﻿namespace RPG_Midterm
{
    partial class DeathScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.deathLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.restartBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // deathLabel
            // 
            this.deathLabel.BackColor = System.Drawing.Color.Transparent;
            this.deathLabel.Font = new System.Drawing.Font("Chiller", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deathLabel.Location = new System.Drawing.Point(12, 42);
            this.deathLabel.Name = "deathLabel";
            this.deathLabel.Size = new System.Drawing.Size(1196, 152);
            this.deathLabel.TabIndex = 0;
            this.deathLabel.Text = "You fought valiantly, but were eventually overrun by the evils of Holding...";
            this.deathLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Chiller", 150F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(12, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1196, 207);
            this.label1.TabIndex = 1;
            this.label1.Text = "YOU DIED!";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // restartBtn
            // 
            this.restartBtn.BackColor = System.Drawing.SystemColors.Control;
            this.restartBtn.Font = new System.Drawing.Font("Chiller", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restartBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.restartBtn.Location = new System.Drawing.Point(270, 437);
            this.restartBtn.Name = "restartBtn";
            this.restartBtn.Size = new System.Drawing.Size(669, 159);
            this.restartBtn.TabIndex = 2;
            this.restartBtn.Text = "Rise From The Grave?";
            this.restartBtn.UseVisualStyleBackColor = false;
            this.restartBtn.Click += new System.EventHandler(this.restartBtn_Click);
            // 
            // DeathScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RPG_Midterm.Properties.Resources.deathback1;
            this.ClientSize = new System.Drawing.Size(1220, 668);
            this.Controls.Add(this.restartBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.deathLabel);
            this.Name = "DeathScreen";
            this.Text = "Form4";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label deathLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button restartBtn;
    }
}