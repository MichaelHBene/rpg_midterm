﻿namespace RPG_Midterm
{
    partial class InventoryScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.characterSheetInventory = new System.Windows.Forms.ListBox();
            this.fightBtn = new System.Windows.Forms.Button();
            this.equippedItems = new System.Windows.Forms.ListBox();
            this.equipBtn = new System.Windows.Forms.Button();
            this.dropBagBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.capacityLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // characterSheetInventory
            // 
            this.characterSheetInventory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.characterSheetInventory.Font = new System.Drawing.Font("DK Northumbria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.characterSheetInventory.FormattingEnabled = true;
            this.characterSheetInventory.ItemHeight = 25;
            this.characterSheetInventory.Location = new System.Drawing.Point(20, 129);
            this.characterSheetInventory.Name = "characterSheetInventory";
            this.characterSheetInventory.Size = new System.Drawing.Size(468, 350);
            this.characterSheetInventory.TabIndex = 1;
            // 
            // fightBtn
            // 
            this.fightBtn.Font = new System.Drawing.Font("DK Northumbria", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fightBtn.Location = new System.Drawing.Point(775, 517);
            this.fightBtn.Name = "fightBtn";
            this.fightBtn.Size = new System.Drawing.Size(445, 90);
            this.fightBtn.TabIndex = 5;
            this.fightBtn.Text = "SEEK ADVENTURE!";
            this.fightBtn.UseVisualStyleBackColor = true;
            this.fightBtn.Click += new System.EventHandler(this.fightBtn_Click);
            // 
            // equippedItems
            // 
            this.equippedItems.BackColor = System.Drawing.SystemColors.Window;
            this.equippedItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.equippedItems.Font = new System.Drawing.Font("DK Northumbria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equippedItems.FormattingEnabled = true;
            this.equippedItems.ItemHeight = 25;
            this.equippedItems.Location = new System.Drawing.Point(724, 129);
            this.equippedItems.Name = "equippedItems";
            this.equippedItems.Size = new System.Drawing.Size(496, 350);
            this.equippedItems.TabIndex = 13;
            // 
            // equipBtn
            // 
            this.equipBtn.Font = new System.Drawing.Font("DK Northumbria", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipBtn.Location = new System.Drawing.Point(20, 517);
            this.equipBtn.Name = "equipBtn";
            this.equipBtn.Size = new System.Drawing.Size(416, 42);
            this.equipBtn.TabIndex = 15;
            this.equipBtn.Text = "Equip Selected";
            this.equipBtn.UseVisualStyleBackColor = true;
            this.equipBtn.Click += new System.EventHandler(this.equipBtn_Click);
            // 
            // dropBagBtn
            // 
            this.dropBagBtn.Font = new System.Drawing.Font("DK Northumbria", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropBagBtn.Location = new System.Drawing.Point(20, 565);
            this.dropBagBtn.Name = "dropBagBtn";
            this.dropBagBtn.Size = new System.Drawing.Size(416, 42);
            this.dropBagBtn.TabIndex = 14;
            this.dropBagBtn.Text = "Drop Selected";
            this.dropBagBtn.UseVisualStyleBackColor = true;
            this.dropBagBtn.Click += new System.EventHandler(this.dropBagBtn_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("DK Northumbria", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(310, -10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(624, 97);
            this.label1.TabIndex = 18;
            this.label1.Text = "Inventory Sheet";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::RPG_Midterm.Properties.Resources.backpack;
            this.pictureBox1.Location = new System.Drawing.Point(494, 90);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(224, 407);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // capacityLabel
            // 
            this.capacityLabel.AutoSize = true;
            this.capacityLabel.BackColor = System.Drawing.Color.Transparent;
            this.capacityLabel.Font = new System.Drawing.Font("DK Northumbria", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capacityLabel.Location = new System.Drawing.Point(14, 90);
            this.capacityLabel.Name = "capacityLabel";
            this.capacityLabel.Size = new System.Drawing.Size(188, 35);
            this.capacityLabel.TabIndex = 20;
            this.capacityLabel.Text = "Bag Items: 0/20";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("DK Northumbria", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(881, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(190, 35);
            this.label3.TabIndex = 21;
            this.label3.Text = "Equipped Items:";
            // 
            // InventoryScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::RPG_Midterm.Properties.Resources.parchback;
            this.ClientSize = new System.Drawing.Size(1232, 629);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.capacityLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.equipBtn);
            this.Controls.Add(this.dropBagBtn);
            this.Controls.Add(this.equippedItems);
            this.Controls.Add(this.fightBtn);
            this.Controls.Add(this.characterSheetInventory);
            this.Name = "InventoryScreen";
            this.Text = "Character Sheet";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox characterSheetInventory;
        private System.Windows.Forms.Button fightBtn;
        private System.Windows.Forms.ListBox equippedItems;
        private System.Windows.Forms.Button equipBtn;
        private System.Windows.Forms.Button dropBagBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label capacityLabel;
        private System.Windows.Forms.Label label3;
    }
}