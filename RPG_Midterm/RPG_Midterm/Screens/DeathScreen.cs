﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPG_Midterm
{
    public partial class DeathScreen : Form
    {
        public DeathScreen()
        {
            InitializeComponent();
        }

        private void restartBtn_Click(object sender, EventArgs e)
        {
            Program.form1 = new MainMenu();
            Program.form1.Show();
            this.Dispose();
        }
    }
}
