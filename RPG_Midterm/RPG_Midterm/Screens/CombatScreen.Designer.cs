﻿namespace RPG_Midterm
{
    partial class CombatScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.meleeAttackBtn = new System.Windows.Forms.Button();
            this.spellAttackBtn = new System.Windows.Forms.Button();
            this.fleeCombatBtn = new System.Windows.Forms.Button();
            this.potion1Btn = new System.Windows.Forms.Button();
            this.combatLogBox = new System.Windows.Forms.TextBox();
            this.playerHealthBar = new System.Windows.Forms.ProgressBar();
            this.enemyHealthBar = new System.Windows.Forms.ProgressBar();
            this.playerHealthLabel = new System.Windows.Forms.Label();
            this.enemyHealthLabel = new System.Windows.Forms.Label();
            this.enemyCombatDisplay = new System.Windows.Forms.PictureBox();
            this.playerCombatDisplay = new System.Windows.Forms.PictureBox();
            this.characterNameLabel = new System.Windows.Forms.Label();
            this.enemyNameLabel = new System.Windows.Forms.Label();
            this.combatInventoryBtn = new System.Windows.Forms.Button();
            this.advanceBtn = new System.Windows.Forms.Button();
            this.enemyEquipBox = new System.Windows.Forms.ListBox();
            this.takeBtn = new System.Windows.Forms.Button();
            this.playerEquipBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LevelDisplayLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.enemyCombatDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCombatDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Moria", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1200, 55);
            this.label1.TabIndex = 2;
            this.label1.Text = "Fight for Your Life!";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // meleeAttackBtn
            // 
            this.meleeAttackBtn.Font = new System.Drawing.Font("Moria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meleeAttackBtn.Location = new System.Drawing.Point(388, 562);
            this.meleeAttackBtn.Name = "meleeAttackBtn";
            this.meleeAttackBtn.Size = new System.Drawing.Size(218, 38);
            this.meleeAttackBtn.TabIndex = 3;
            this.meleeAttackBtn.Text = "Melee Attack";
            this.meleeAttackBtn.UseVisualStyleBackColor = true;
            this.meleeAttackBtn.Click += new System.EventHandler(this.meleeAttackBtn_Click);
            // 
            // spellAttackBtn
            // 
            this.spellAttackBtn.Font = new System.Drawing.Font("Moria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spellAttackBtn.Location = new System.Drawing.Point(612, 562);
            this.spellAttackBtn.Name = "spellAttackBtn";
            this.spellAttackBtn.Size = new System.Drawing.Size(218, 38);
            this.spellAttackBtn.TabIndex = 4;
            this.spellAttackBtn.Text = "Spell 3/3";
            this.spellAttackBtn.UseVisualStyleBackColor = true;
            this.spellAttackBtn.Click += new System.EventHandler(this.spellAttackBtn_Click);
            // 
            // fleeCombatBtn
            // 
            this.fleeCombatBtn.Font = new System.Drawing.Font("Moria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fleeCombatBtn.Location = new System.Drawing.Point(612, 606);
            this.fleeCombatBtn.Name = "fleeCombatBtn";
            this.fleeCombatBtn.Size = new System.Drawing.Size(218, 38);
            this.fleeCombatBtn.TabIndex = 5;
            this.fleeCombatBtn.Text = "Flee Combat";
            this.fleeCombatBtn.UseVisualStyleBackColor = true;
            this.fleeCombatBtn.Click += new System.EventHandler(this.fleeCombatBtn_Click);
            // 
            // potion1Btn
            // 
            this.potion1Btn.Font = new System.Drawing.Font("Moria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.potion1Btn.Location = new System.Drawing.Point(388, 606);
            this.potion1Btn.Name = "potion1Btn";
            this.potion1Btn.Size = new System.Drawing.Size(218, 38);
            this.potion1Btn.TabIndex = 6;
            this.potion1Btn.Text = "Potion";
            this.potion1Btn.UseVisualStyleBackColor = true;
            this.potion1Btn.Click += new System.EventHandler(this.potion1Btn_Click);
            // 
            // combatLogBox
            // 
            this.combatLogBox.Font = new System.Drawing.Font("Moria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combatLogBox.Location = new System.Drawing.Point(372, 89);
            this.combatLogBox.Multiline = true;
            this.combatLogBox.Name = "combatLogBox";
            this.combatLogBox.ReadOnly = true;
            this.combatLogBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.combatLogBox.Size = new System.Drawing.Size(458, 328);
            this.combatLogBox.TabIndex = 8;
            this.combatLogBox.Text = "Combat:\r\n---------------------";
            // 
            // playerHealthBar
            // 
            this.playerHealthBar.Location = new System.Drawing.Point(39, 482);
            this.playerHealthBar.Name = "playerHealthBar";
            this.playerHealthBar.Size = new System.Drawing.Size(322, 48);
            this.playerHealthBar.TabIndex = 9;
            this.playerHealthBar.Value = 100;
            // 
            // enemyHealthBar
            // 
            this.enemyHealthBar.Location = new System.Drawing.Point(864, 482);
            this.enemyHealthBar.Name = "enemyHealthBar";
            this.enemyHealthBar.Size = new System.Drawing.Size(322, 48);
            this.enemyHealthBar.TabIndex = 10;
            this.enemyHealthBar.Value = 100;
            // 
            // playerHealthLabel
            // 
            this.playerHealthLabel.BackColor = System.Drawing.Color.Transparent;
            this.playerHealthLabel.Font = new System.Drawing.Font("Moria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerHealthLabel.Location = new System.Drawing.Point(36, 482);
            this.playerHealthLabel.Name = "playerHealthLabel";
            this.playerHealthLabel.Size = new System.Drawing.Size(325, 31);
            this.playerHealthLabel.TabIndex = 11;
            this.playerHealthLabel.Text = "Player Health: 100/100";
            this.playerHealthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enemyHealthLabel
            // 
            this.enemyHealthLabel.BackColor = System.Drawing.Color.Transparent;
            this.enemyHealthLabel.Font = new System.Drawing.Font("Moria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyHealthLabel.Location = new System.Drawing.Point(860, 482);
            this.enemyHealthLabel.Name = "enemyHealthLabel";
            this.enemyHealthLabel.Size = new System.Drawing.Size(325, 31);
            this.enemyHealthLabel.TabIndex = 12;
            this.enemyHealthLabel.Text = "Enemy Health: 100/100";
            this.enemyHealthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enemyCombatDisplay
            // 
            this.enemyCombatDisplay.ErrorImage = null;
            this.enemyCombatDisplay.Location = new System.Drawing.Point(886, 89);
            this.enemyCombatDisplay.Name = "enemyCombatDisplay";
            this.enemyCombatDisplay.Size = new System.Drawing.Size(259, 328);
            this.enemyCombatDisplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.enemyCombatDisplay.TabIndex = 1;
            this.enemyCombatDisplay.TabStop = false;
            // 
            // playerCombatDisplay
            // 
            this.playerCombatDisplay.Location = new System.Drawing.Point(65, 89);
            this.playerCombatDisplay.Name = "playerCombatDisplay";
            this.playerCombatDisplay.Size = new System.Drawing.Size(259, 328);
            this.playerCombatDisplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.playerCombatDisplay.TabIndex = 0;
            this.playerCombatDisplay.TabStop = false;
            // 
            // characterNameLabel
            // 
            this.characterNameLabel.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.characterNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.characterNameLabel.Font = new System.Drawing.Font("Moria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.characterNameLabel.Location = new System.Drawing.Point(36, 422);
            this.characterNameLabel.Name = "characterNameLabel";
            this.characterNameLabel.Size = new System.Drawing.Size(322, 60);
            this.characterNameLabel.TabIndex = 13;
            this.characterNameLabel.Text = "CharacterNameHere";
            this.characterNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // enemyNameLabel
            // 
            this.enemyNameLabel.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.enemyNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.enemyNameLabel.Font = new System.Drawing.Font("Moria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyNameLabel.Location = new System.Drawing.Point(860, 422);
            this.enemyNameLabel.Name = "enemyNameLabel";
            this.enemyNameLabel.Size = new System.Drawing.Size(322, 60);
            this.enemyNameLabel.TabIndex = 14;
            this.enemyNameLabel.Text = "EnemyNameHere";
            this.enemyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // combatInventoryBtn
            // 
            this.combatInventoryBtn.Font = new System.Drawing.Font("Moria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combatInventoryBtn.Location = new System.Drawing.Point(612, 423);
            this.combatInventoryBtn.Name = "combatInventoryBtn";
            this.combatInventoryBtn.Size = new System.Drawing.Size(237, 56);
            this.combatInventoryBtn.TabIndex = 15;
            this.combatInventoryBtn.Text = "Inventory";
            this.combatInventoryBtn.UseVisualStyleBackColor = true;
            this.combatInventoryBtn.Visible = false;
            this.combatInventoryBtn.Click += new System.EventHandler(this.combatInventoryBtn_Click);
            // 
            // advanceBtn
            // 
            this.advanceBtn.Font = new System.Drawing.Font("Moria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advanceBtn.Location = new System.Drawing.Point(369, 423);
            this.advanceBtn.Name = "advanceBtn";
            this.advanceBtn.Size = new System.Drawing.Size(237, 56);
            this.advanceBtn.TabIndex = 16;
            this.advanceBtn.Text = "Journey On";
            this.advanceBtn.UseVisualStyleBackColor = true;
            this.advanceBtn.Visible = false;
            this.advanceBtn.Click += new System.EventHandler(this.advanceBtn_Click);
            // 
            // enemyEquipBox
            // 
            this.enemyEquipBox.Font = new System.Drawing.Font("Moria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyEquipBox.FormattingEnabled = true;
            this.enemyEquipBox.ItemHeight = 19;
            this.enemyEquipBox.Location = new System.Drawing.Point(864, 542);
            this.enemyEquipBox.Name = "enemyEquipBox";
            this.enemyEquipBox.Size = new System.Drawing.Size(322, 118);
            this.enemyEquipBox.TabIndex = 17;
            // 
            // takeBtn
            // 
            this.takeBtn.Font = new System.Drawing.Font("Moria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.takeBtn.Location = new System.Drawing.Point(380, 485);
            this.takeBtn.Name = "takeBtn";
            this.takeBtn.Size = new System.Drawing.Size(458, 45);
            this.takeBtn.TabIndex = 19;
            this.takeBtn.Text = "Take Selected Loot";
            this.takeBtn.UseVisualStyleBackColor = true;
            this.takeBtn.Visible = false;
            this.takeBtn.Click += new System.EventHandler(this.takeBtn_Click);
            // 
            // playerEquipBox
            // 
            this.playerEquipBox.Font = new System.Drawing.Font("Moria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerEquipBox.FormattingEnabled = true;
            this.playerEquipBox.ItemHeight = 19;
            this.playerEquipBox.Location = new System.Drawing.Point(36, 542);
            this.playerEquipBox.Name = "playerEquipBox";
            this.playerEquipBox.Size = new System.Drawing.Size(322, 118);
            this.playerEquipBox.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "label2";
            // 
            // LevelDisplayLabel
            // 
            this.LevelDisplayLabel.BackColor = System.Drawing.Color.Transparent;
            this.LevelDisplayLabel.Font = new System.Drawing.Font("Moria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelDisplayLabel.Location = new System.Drawing.Point(12, 64);
            this.LevelDisplayLabel.Name = "LevelDisplayLabel";
            this.LevelDisplayLabel.Size = new System.Drawing.Size(1200, 23);
            this.LevelDisplayLabel.TabIndex = 22;
            this.LevelDisplayLabel.Text = "Dungeon Depth: 1";
            this.LevelDisplayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CombatScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RPG_Midterm.Properties.Resources.parchback;
            this.ClientSize = new System.Drawing.Size(1224, 670);
            this.Controls.Add(this.LevelDisplayLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.playerEquipBox);
            this.Controls.Add(this.takeBtn);
            this.Controls.Add(this.enemyEquipBox);
            this.Controls.Add(this.advanceBtn);
            this.Controls.Add(this.combatInventoryBtn);
            this.Controls.Add(this.enemyNameLabel);
            this.Controls.Add(this.characterNameLabel);
            this.Controls.Add(this.enemyHealthLabel);
            this.Controls.Add(this.playerHealthLabel);
            this.Controls.Add(this.enemyHealthBar);
            this.Controls.Add(this.playerHealthBar);
            this.Controls.Add(this.combatLogBox);
            this.Controls.Add(this.potion1Btn);
            this.Controls.Add(this.fleeCombatBtn);
            this.Controls.Add(this.spellAttackBtn);
            this.Controls.Add(this.meleeAttackBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.enemyCombatDisplay);
            this.Controls.Add(this.playerCombatDisplay);
            this.Name = "CombatScreen";
            this.Text = "Combat";
            ((System.ComponentModel.ISupportInitialize)(this.enemyCombatDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCombatDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox playerCombatDisplay;
        private System.Windows.Forms.PictureBox enemyCombatDisplay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button meleeAttackBtn;
        private System.Windows.Forms.Button spellAttackBtn;
        private System.Windows.Forms.Button fleeCombatBtn;
        private System.Windows.Forms.Button potion1Btn;
        private System.Windows.Forms.TextBox combatLogBox;
        private System.Windows.Forms.ProgressBar playerHealthBar;
        private System.Windows.Forms.ProgressBar enemyHealthBar;
        private System.Windows.Forms.Label playerHealthLabel;
        private System.Windows.Forms.Label enemyHealthLabel;
        private System.Windows.Forms.Label characterNameLabel;
        private System.Windows.Forms.Label enemyNameLabel;
        private System.Windows.Forms.Button combatInventoryBtn;
        private System.Windows.Forms.Button advanceBtn;
        private System.Windows.Forms.ListBox enemyEquipBox;
        private System.Windows.Forms.Button takeBtn;
        private System.Windows.Forms.ListBox playerEquipBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LevelDisplayLabel;
    }
}