﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPG_Midterm
{
    public partial class InventoryScreen : Form
    {
        private GameManager _gm;

        public InventoryScreen(GameManager gm)
        {
            InitializeComponent();
            this._gm = gm;
            Program.form3 = new CombatScreen(_gm);
            UpdateInventoryList();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void fightBtn_Click(object sender, EventArgs e)
        {
            Program.form3.UpdateInventory();
            Program.form3.Show();
            this.Hide();
            
        }

        public void UpdateInventoryList()
        {
            characterSheetInventory.Items.Clear();
            equippedItems.Items.Clear();
            for (int x = 0; x < _gm.Player.Bag.ItemCount; ++x)
            {
                Item selectedItem = _gm.Player.Bag.GetItem(x);
                if (selectedItem != null)
                {
                    characterSheetInventory.Items.Add(selectedItem);
                }
            }

            for (int x = 0; x < 6; ++x)
            {
                if (_gm.Player.Equipped.GetItem((InventorySlotId)x) != null)
                {
                    equippedItems.Items.Add(_gm.Player.Equipped.GetItem((InventorySlotId)x));
                }
            }

            capacityLabel.Text = _gm.Player.Bag.ItemCount + " / 20";

        }

        private void equipBtn_Click(object sender, EventArgs e)
        {
            if(characterSheetInventory.SelectedIndex != -1)
            {
                if (_gm.Player.Equipped.GetItem(_gm.Player.Bag.GetItem(characterSheetInventory.SelectedIndex).Slot) != null)
                {
                    Item equippingItem = _gm.Player.Bag.GetItem(characterSheetInventory.SelectedIndex);

                    _gm.Player.Bag.RemoveFromInventory(characterSheetInventory.SelectedIndex);

                    Item unequippedItem = _gm.Player.Equipped.Equip(equippingItem);

                    _gm.Player.Bag.AddToInventory(unequippedItem);
                }
                else
                {
                    Item equippingItem = _gm.Player.Bag.GetItem(characterSheetInventory.SelectedIndex);

                    _gm.Player.Bag.RemoveFromInventory(characterSheetInventory.SelectedIndex);

                    _gm.Player.Equipped.Equip(equippingItem);
                }
            }
            UpdateInventoryList();
        }

        private void dropBagBtn_Click(object sender, EventArgs e)
        {
            Item droppingItem = _gm.Player.Bag.GetItem(characterSheetInventory.SelectedIndex);
            _gm.Player.Bag.RemoveFromInventory(characterSheetInventory.SelectedIndex);
            UpdateInventoryList();
        }
    }
}
