﻿namespace RPG_Midterm
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CharacterNameInput = new System.Windows.Forms.TextBox();
            this.CharacterScreenTitle = new System.Windows.Forms.Label();
            this.AdventureBtn = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.CharacterImageBox = new System.Windows.Forms.PictureBox();
            this.NinjaClassSelect = new System.Windows.Forms.RadioButton();
            this.BardClassSelect = new System.Windows.Forms.RadioButton();
            this.PastaClassSelector = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.CharacterImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Papyrus", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(489, 300);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 76);
            this.label5.TabIndex = 22;
            this.label5.Text = "Class:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Papyrus", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(489, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 76);
            this.label2.TabIndex = 17;
            this.label2.Text = "Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Papyrus", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(493, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(645, 51);
            this.label1.TabIndex = 16;
            this.label1.Text = "Build your Character, Champion of Holding!";
            // 
            // CharacterNameInput
            // 
            this.CharacterNameInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CharacterNameInput.Location = new System.Drawing.Point(727, 246);
            this.CharacterNameInput.Name = "CharacterNameInput";
            this.CharacterNameInput.Size = new System.Drawing.Size(398, 38);
            this.CharacterNameInput.TabIndex = 15;
            // 
            // CharacterScreenTitle
            // 
            this.CharacterScreenTitle.AutoSize = true;
            this.CharacterScreenTitle.BackColor = System.Drawing.Color.Transparent;
            this.CharacterScreenTitle.Font = new System.Drawing.Font("Moria", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CharacterScreenTitle.Location = new System.Drawing.Point(44, 28);
            this.CharacterScreenTitle.Name = "CharacterScreenTitle";
            this.CharacterScreenTitle.Size = new System.Drawing.Size(1149, 77);
            this.CharacterScreenTitle.TabIndex = 13;
            this.CharacterScreenTitle.Text = "Adventures of Holding";
            this.CharacterScreenTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AdventureBtn
            // 
            this.AdventureBtn.Font = new System.Drawing.Font("Moria", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdventureBtn.Location = new System.Drawing.Point(502, 394);
            this.AdventureBtn.Name = "AdventureBtn";
            this.AdventureBtn.Size = new System.Drawing.Size(636, 81);
            this.AdventureBtn.TabIndex = 27;
            this.AdventureBtn.Text = "Begin Thine Adventure, Hero!";
            this.AdventureBtn.UseVisualStyleBackColor = true;
            this.AdventureBtn.Click += new System.EventHandler(this.AdventureBtn_Click);
            // 
            // CharacterImageBox
            // 
            this.CharacterImageBox.BackColor = System.Drawing.Color.Transparent;
            this.CharacterImageBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CharacterImageBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CharacterImageBox.Image = global::RPG_Midterm.Properties.Resources.Pastromancer1;
            this.CharacterImageBox.InitialImage = null;
            this.CharacterImageBox.Location = new System.Drawing.Point(57, 118);
            this.CharacterImageBox.Name = "CharacterImageBox";
            this.CharacterImageBox.Size = new System.Drawing.Size(370, 473);
            this.CharacterImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.CharacterImageBox.TabIndex = 14;
            this.CharacterImageBox.TabStop = false;
            // 
            // NinjaClassSelect
            // 
            this.NinjaClassSelect.AutoSize = true;
            this.NinjaClassSelect.BackColor = System.Drawing.Color.Transparent;
            this.NinjaClassSelect.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NinjaClassSelect.Location = new System.Drawing.Point(971, 332);
            this.NinjaClassSelect.Name = "NinjaClassSelect";
            this.NinjaClassSelect.Size = new System.Drawing.Size(128, 29);
            this.NinjaClassSelect.TabIndex = 25;
            this.NinjaClassSelect.Text = "Cyborg Ninja";
            this.NinjaClassSelect.UseVisualStyleBackColor = false;
            this.NinjaClassSelect.CheckedChanged += new System.EventHandler(this.NinjaClassSelect_CheckedChanged);
            // 
            // BardClassSelect
            // 
            this.BardClassSelect.AutoSize = true;
            this.BardClassSelect.BackColor = System.Drawing.Color.Transparent;
            this.BardClassSelect.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BardClassSelect.Location = new System.Drawing.Point(855, 332);
            this.BardClassSelect.Name = "BardClassSelect";
            this.BardClassSelect.Size = new System.Drawing.Size(110, 29);
            this.BardClassSelect.TabIndex = 24;
            this.BardClassSelect.Text = "Bardbarian";
            this.BardClassSelect.UseVisualStyleBackColor = false;
            this.BardClassSelect.CheckedChanged += new System.EventHandler(this.BardClassSelect_CheckedChanged);
            // 
            // PastaClassSelector
            // 
            this.PastaClassSelector.AutoSize = true;
            this.PastaClassSelector.BackColor = System.Drawing.Color.Transparent;
            this.PastaClassSelector.Checked = true;
            this.PastaClassSelector.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PastaClassSelector.Location = new System.Drawing.Point(727, 332);
            this.PastaClassSelector.Name = "PastaClassSelector";
            this.PastaClassSelector.Size = new System.Drawing.Size(122, 29);
            this.PastaClassSelector.TabIndex = 23;
            this.PastaClassSelector.TabStop = true;
            this.PastaClassSelector.Text = "Pastromancer";
            this.PastaClassSelector.UseVisualStyleBackColor = false;
            this.PastaClassSelector.CheckedChanged += new System.EventHandler(this.PastaClassSelector_CheckedChanged);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::RPG_Midterm.Properties.Resources.parchback;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1237, 628);
            this.Controls.Add(this.PastaClassSelector);
            this.Controls.Add(this.AdventureBtn);
            this.Controls.Add(this.BardClassSelect);
            this.Controls.Add(this.NinjaClassSelect);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CharacterNameInput);
            this.Controls.Add(this.CharacterImageBox);
            this.Controls.Add(this.CharacterScreenTitle);
            this.Name = "MainMenu";
            this.Text = "Character Selection";
            ((System.ComponentModel.ISupportInitialize)(this.CharacterImageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox CharacterNameInput;
        public System.Windows.Forms.PictureBox CharacterImageBox;
        private System.Windows.Forms.Label CharacterScreenTitle;
        private System.Windows.Forms.Button AdventureBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RadioButton NinjaClassSelect;
        private System.Windows.Forms.RadioButton BardClassSelect;
        private System.Windows.Forms.RadioButton PastaClassSelector;
    }
}

