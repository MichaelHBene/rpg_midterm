﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPG_Midterm
{
    public partial class MainMenu : Form
    {
        private GameManager _gm;

        public MainMenu()
        {
            InitializeComponent();
            _gm = new GameManager();
        }

        private void PastaClassSelector_CheckedChanged(object sender, EventArgs e)
        {
            //Changes character image when class is picked
            CharacterImageBox.Image = RPG_Midterm.Properties.Resources.Pastromancer1;
        }

        private void BardClassSelect_CheckedChanged(object sender, EventArgs e)
        {
            //Changes character image when class is picked
            CharacterImageBox.Image = RPG_Midterm.Properties.Resources.Bardbarian1;
        }

        private void NinjaClassSelect_CheckedChanged(object sender, EventArgs e)
        {
            //Changes character image when class is picked
            CharacterImageBox.Image = RPG_Midterm.Properties.Resources.CyborgNinja1;
        }

        private void AdventureBtn_Click(object sender, EventArgs e)
        {
            //Creates a new character object after clicking the adventure button
            Player player = new Player();
            //player.Name = CharacterNameInput.Text;



            //Opens form2 and closes form1 when button is clicked

            Program.form2 = new InventoryScreen(_gm);
            Program.form2.Show();
            this.Hide();

        }

    }
}
