﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPG_Midterm
{
    public partial class CombatScreen : Form
    {
        public static System.Media.SoundPlayer combatPlayer;
        public static System.Media.SoundPlayer damagePlayer;
        private GameManager _gm;
        public CombatScreen(GameManager gm)
        {
            combatPlayer = new System.Media.SoundPlayer(@"C:\Users\micha\Documents\RPG_Midterm\sounds\Death_and_Axes.wav");
            damagePlayer = new System.Media.SoundPlayer(@"C:\Users\micha\Documents\RPG_Midterm\sounds\BeepBox-Song.wav");
            //Program.player.Stop();
            combatPlayer.Play();

            InitializeComponent();
            
            this._gm = gm;
            //Sets Player Image and Name
            playerCombatDisplay.Image = Program.form1.CharacterImageBox.Image;
            characterNameLabel.Text = Program.form1.CharacterNameInput.Text;

            //Update Enemy Displays
            enemyCombatDisplay.Image = _gm.Enemy.Image;
            enemyNameLabel.Text = _gm.Enemy.Name;
            enemyHealthBar.Maximum = _gm.Enemy.MaxHealth;
            enemyHealthBar.Value = _gm.Enemy.MaxHealth;
            enemyHealthLabel.Text = "Enemy Health: " + _gm.Enemy.CurrentHealth + "/" + _gm.Enemy.MaxHealth;
            UpdateInventory();
        } 

        private void meleeAttackBtn_Click(object sender, EventArgs e)
        {
            //Generates random attack statements for the combat log
            Random randAttackStatement = new Random();

            int statement = randAttackStatement.Next(1, 5);
            int statement2 = randAttackStatement.Next(1, 5);
            string attackStatement = "";
            string enemyStatement = "";

            switch (statement)
            {
                case 1:
                    attackStatement = " lashes out at the enemy for ";
                    break;
                case 2:
                    attackStatement = " swings with might at the enemy for ";
                    break;
                case 3:
                    attackStatement = " flies into a frenzy and attacks the enemy for ";
                    break;
                case 4:
                    attackStatement = " attacks with the full force of their weapon for ";
                    break;

                default:
                    break;

            }
            switch (statement2)
            {
                case 1:
                    enemyStatement = " lashes out at the enemy for ";
                    break;
                case 2:
                    enemyStatement = " swings with might at the enemy for ";
                    break;
                case 3:
                    enemyStatement = " flies into a frenzy and attacks the enemy for ";
                    break;
                case 4:
                    enemyStatement = " attacks with the full force of their weapon for ";
                    break;

                default:
                    break;

            }
            //Populates combat log and subtracts from character healths

            combatLogBox.Text += ("\r\n" + characterNameLabel.Text + attackStatement + _gm.Attack(_gm.Player, _gm.Enemy) + " points of damage!\r\n");
            enemyHealthBar.Value =_gm.Enemy.CurrentHealth;
            enemyHealthLabel.Text = "Enemy Health: " + _gm.Enemy.CurrentHealth + "/" + _gm.Enemy.MaxHealth;

            if (_gm.Enemy.IsDead)
            {
                for (int x = 0; x < 6; ++x)
                {
                    if (_gm.Enemy.Equipped.GetItem((InventorySlotId)x) != null)
                    {
                        if (_gm.Enemy.Equipped.GetItem((InventorySlotId)x) is IArmor)
                        {
                            IArmor temp = (IArmor)_gm.Enemy.Equipped.GetItem((InventorySlotId)x);
                            if (!temp.IsNatural)
                            {
                                _gm.Enemy.Bag.AddToInventory(_gm.Enemy.Equipped.GetItem((InventorySlotId)x));
                            }
                        }
                        else if (_gm.Enemy.Equipped.GetItem((InventorySlotId)x) is IWeapon)
                        {
                            IWeapon temp = (IWeapon)_gm.Enemy.Equipped.GetItem((InventorySlotId)x);
                            if (!temp.IsNatural)
                            {
                                _gm.Enemy.Bag.AddToInventory(_gm.Enemy.Equipped.GetItem((InventorySlotId)x));
                            }
                        }
                        else
                        {
                            _gm.Enemy.Bag.AddToInventory(_gm.Enemy.Equipped.GetItem((InventorySlotId)x));
                        }
                    }
                }
                UpdateInventory();
                meleeAttackBtn.Visible = false;
                spellAttackBtn.Visible = false;
                potion1Btn.Visible = false;
                fleeCombatBtn.Visible = false;
                advanceBtn.Visible = true;
                combatInventoryBtn.Visible = true;
                takeBtn.Visible = true;
                return;
            }

            combatLogBox.Text += ("\r\n" + enemyNameLabel.Text + enemyStatement + _gm.Attack(_gm.Enemy, _gm.Player) + " points of damage!\r\n");
            playerHealthBar.Value = _gm.Player.CurrentHealth;
            playerHealthLabel.Text = "Player Health: " + _gm.Player.CurrentHealth + "/" + _gm.Player.MaxHealth;
            damagePlayer.Play();

            if (_gm.Player.IsDead)
            {
                DeathScreen deathScreen = new DeathScreen();
                deathScreen.Show();
                this.Hide();
            }



        }

        //Cast spell
        private void spellAttackBtn_Click(object sender, EventArgs e)
        {
            combatLogBox.Text += ("\r\n" + characterNameLabel.Text + " conjures up a damaging blast of magic for PLACEHOLDER points of damage!\r\n");
        }

        //Drink potion
        private void potion1Btn_Click(object sender, EventArgs e)
        {
            Item equippedPotion = _gm.Player.Equipped.GetItem((InventorySlotId.POTION1));
            if (equippedPotion != null)
            {
                combatLogBox.Text += ("\r\n" + characterNameLabel.Text + " chugs a healing potion, replenishing themselves for " + _gm.DrinkPotion(_gm.Player, (IPotion)_gm.Player.Equipped.GetItem(InventorySlotId.POTION1)) + " points of healing!\r\n");
                playerHealthBar.Value = _gm.Player.CurrentHealth;
                playerHealthLabel.Text = "Player Health: " + _gm.Player.CurrentHealth + "/" + _gm.Player.MaxHealth;
                equippedPotion = null;
            }
            else if (equippedPotion == null)
            {
                combatLogBox.Text += ("\r\n" + characterNameLabel.Text + " reaches into their belt to pull out a potion, but realizes that they forgot to have one equipped");
            }
        }

        //Attempt to flee
        private void fleeCombatBtn_Click(object sender, EventArgs e)
        {
            combatLogBox.Text += ("\r\n" + characterNameLabel.Text + " attempts to flee combat....\r\n");
            Random randFleeChance = new Random();

            int playerFlee = randFleeChance.Next(1, 3);

            switch (playerFlee)
            {
                case 1:
                    //Player fails
                    combatLogBox.Text += ("\r\n" + characterNameLabel.Text + " fails to flee combat!\r\n");
                    combatLogBox.Text += ("\r\n" + enemyNameLabel.Text + " takes an attack of opportunity, dealing " + _gm.Attack(_gm.Enemy, _gm.Player) + " points of damage!\r\n");
                    playerHealthBar.Value = _gm.Player.CurrentHealth;
                    playerHealthLabel.Text = "Player Health: " + _gm.Player.CurrentHealth + "/" + _gm.Player.MaxHealth;
                    damagePlayer.Play();
                    if (_gm.Player.IsDead)
                    {
                        DeathScreen deathScreen = new DeathScreen();
                        deathScreen.Show();
                        this.Hide();
                    }
                    break;
                case 2:
                    //Player succeeds
                    combatLogBox.Text += ("\r\n" + characterNameLabel.Text + " successfully flees combat!\r\n");
                    InventoryScreen InventoryScreen = new InventoryScreen(_gm);
                    InventoryScreen.Show();
                    this.Hide();
                    _gm.NextBattle();

                    enemyNameLabel.Text = _gm.Enemy.Name;
                    enemyHealthBar.Maximum = _gm.Enemy.MaxHealth;
                    enemyHealthBar.Value = _gm.Enemy.MaxHealth;
                    enemyHealthLabel.Text = "Enemy Health: " + _gm.Enemy.CurrentHealth + "/" + _gm.Enemy.MaxHealth;

                    for (int x = 0; x < 6; ++x)
                    {
                        if (_gm.Enemy.Equipped.GetItem((InventorySlotId)x) != null)
                        {
                            enemyEquipBox.Items.Add(_gm.Enemy.Equipped.GetItem((InventorySlotId)x));
                        }
                    }

                    meleeAttackBtn.Visible = true;
                    spellAttackBtn.Visible = true;
                    potion1Btn.Visible = true;
                    fleeCombatBtn.Visible = true;
                    advanceBtn.Visible = false;
                    combatInventoryBtn.Visible = false;
                    takeBtn.Visible = false;
                    break;
                case 3:
                    //Player succeeds
                    combatLogBox.Text += ("\r\n" + characterNameLabel.Text + " expertly flees combat!\r\n");
                    InventoryScreen InventoryScreen2 = new InventoryScreen(_gm);
                    InventoryScreen2.Show();
                    this.Hide();
                    _gm.NextBattle();

                    enemyNameLabel.Text = _gm.Enemy.Name;
                    enemyHealthBar.Maximum = _gm.Enemy.MaxHealth;
                    enemyHealthBar.Value = _gm.Enemy.MaxHealth;
                    enemyHealthLabel.Text = "Enemy Health: " + _gm.Enemy.CurrentHealth + "/" + _gm.Enemy.MaxHealth;

                    for (int x = 0; x < 6; ++x)
                    {
                        if (_gm.Enemy.Equipped.GetItem((InventorySlotId)x) != null)
                        {
                            enemyEquipBox.Items.Add(_gm.Enemy.Equipped.GetItem((InventorySlotId)x));
                        }
                    }

                    meleeAttackBtn.Visible = true;
                    spellAttackBtn.Visible = true;
                    potion1Btn.Visible = true;
                    fleeCombatBtn.Visible = true;
                    advanceBtn.Visible = false;
                    combatInventoryBtn.Visible = false;
                    takeBtn.Visible = false;
                    break;

            }

        }

        //Access inventory after combat
        private void combatInventoryBtn_Click(object sender, EventArgs e)
        {
            //InventoryScreen InventoryScreen = new InventoryScreen(_gm);
            Program.form2.Show();
            Program.form2.UpdateInventoryList();
            this.Hide();
        }

        public void UpdateInventory()
        {
            //Update Enemy Combat Inventory
            enemyEquipBox.Items.Clear();

            //Checks if enemy is dead
            //If so, display the loot
            if (_gm.Enemy.IsDead)
            {
                for (int x = 0; x < _gm.Enemy.Bag.ItemCount; ++x)
                {

                    if (_gm.Enemy.Bag.GetItem(x) != null)
                    {
                        enemyEquipBox.Items.Add(_gm.Enemy.Bag.GetItem(x).ToString());
                    }
                }
            }
            //Else, display their equipped items and bag
            else
            {
                for (int x = 0; x < _gm.Enemy.Bag.ItemCount; ++x)
                {
                    if (_gm.Enemy.Bag.GetItem(x) != null)
                    {
                        enemyEquipBox.Items.Add(_gm.Enemy.Bag.GetItem(x));
                    }
                }
                for (int x = 0; x < 6; ++x)
                {
                    if (_gm.Enemy.Equipped.GetItem((InventorySlotId)x) != null)
                    {
                        enemyEquipBox.Items.Add(_gm.Enemy.Equipped.GetItem((InventorySlotId)x));
                    }
                }
            }
            //Update Player Combat Inventory
            playerEquipBox.Items.Clear();
            for (int x = 0; x < 6; ++x)
            {
                if (_gm.Player.Equipped.GetItem((InventorySlotId)x) != null)
                {
                    playerEquipBox.Items.Add(_gm.Player.Equipped.GetItem((InventorySlotId)x));
                }
            }
        }

        private void advanceBtn_Click(object sender, EventArgs e)
        {
            _gm.NextBattle();
            LevelDisplayLabel.Text = "Dungeon Depth: " + _gm.Depth;
            UpdateInventory();

            combatLogBox.Text = "Combat:\r\n-------------------- ";
            enemyCombatDisplay.Image = _gm.Enemy.Image;
            enemyNameLabel.Text = _gm.Enemy.Name;
            enemyHealthBar.Maximum = _gm.Enemy.MaxHealth;
            enemyHealthBar.Value = _gm.Enemy.MaxHealth;
            enemyHealthLabel.Text = "Enemy Health: " + _gm.Enemy.CurrentHealth + "/" + _gm.Enemy.MaxHealth;

            meleeAttackBtn.Visible = true;
            spellAttackBtn.Visible = true;
            potion1Btn.Visible = true;
            fleeCombatBtn.Visible = true;
            advanceBtn.Visible = false;
            combatInventoryBtn.Visible = false;
            takeBtn.Visible = false;

        }

        private void takeBtn_Click(object sender, EventArgs e)
        {
            if(_gm.Player.Bag.ItemCount != 20)
            {
                int selectedIndex = enemyEquipBox.SelectedIndex;

                if (selectedIndex != -1)
                {
                    Item lootedItem = _gm.Enemy.Bag.GetItem(selectedIndex);

                    _gm.Enemy.Bag.RemoveFromInventory(selectedIndex);

                    _gm.Player.Bag.AddToInventory(lootedItem);
                }
            }
            else
            {
                enemyHealthLabel.Text = characterNameLabel.Text + " says: I am overburdened.";
            }

            UpdateInventory();

            
        }

    }
}
