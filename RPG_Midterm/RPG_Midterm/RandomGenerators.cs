﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    static class RandomGenerators
    {
        static Random rand = new Random();

        public static Enemy GenerateRandomMiniboss()
        {
            int enemyGen = rand.Next(1, 7);
            switch (enemyGen)
            {
                case 1:

                    return new Wyrmling();

                case 2:

                    return new Blight();

                case 3:
                    return new GreatLandKraken();

                default:
                    return new Rat();

            }
        }

        public static Enemy GenerateRandomEnemy()
        {
            int fightDepth = 1;

            int enemyGen = rand.Next(1, 7);

            if((fightDepth % 5) == 0)
            {
                switch (enemyGen)
                {
                    case 1:
                        ++fightDepth;
                        return new Wyrmling();

                    case 2:
                        ++fightDepth;
                        return new Blight();

                    case 3:
                        ++fightDepth;
                        return new GreatLandKraken();

                    default:
                        ++fightDepth;
                        return new Rat();

                }
                
            }
            else
            {
                switch (enemyGen)
                {
                    case 1:
                        ++fightDepth;
                        return new Rat();
                        
                    case 2:
                        ++fightDepth;
                        return new Dawg();

                    case 3:
                        ++fightDepth;
                        return new Bat();

                    case 4:
                        ++fightDepth;
                        return new Kobold();

                    case 5:
                        ++fightDepth;
                        return new Goblin();

                    case 6:
                        ++fightDepth;
                        return new Orc();

                    default:
                        ++fightDepth;
                        return new Rat();

                }
                
            }
            
        }

        public static Item GenerateRandomItem()
        {
            int itemGen = rand.Next(1, 20);

            switch (itemGen)
            {
                case 1:
                    return new Gambeson();
                case 2:
                    return new IronChestpiece();
                case 3:
                    return new SteelChestpiece();
                case 4:
                    return new BronzeHelmet();
                case 5:
                    return new IronHelmet();
                case 6:
                    return new SpikeyHelmet();
                case 7:
                    return new SteelHelmet();
                case 8:
                    return new SmallHealthPotion();
                case 9:
                    return new LargeHealthPotion();
                case 10:
                    return new BronzeVambraces();
                case 11:
                    return new IronVambraces();
                case 12:
                    return new SteelVambraces();
                case 13:
                    return new BronzeGrieves();
                case 14:
                    return new IronGrieves();
                case 15:
                    return new SteelGrieves();
                case 16:
                    return new BronzeSword();
                case 17:
                    return new Buckler();
                case 18:
                    return new IronSword();
                case 19:
                    return new SteelSword();
                default:
                    return new Gambeson();
            }
        }
    }
}
