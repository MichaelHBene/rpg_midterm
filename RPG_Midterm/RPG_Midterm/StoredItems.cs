﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class StoredItems
    {
        private Item[] _items;
        private int _itemCount;
        private int _size;

        public int ItemCount { get { return _itemCount; } }
        public int Size { get { return _size; } }

        public StoredItems(int bagSize)
        {
            _items = new Item[bagSize];
            _size = bagSize;
            _itemCount = 0;
        }

        public StoredItems(int bagSize, int rand) : this(bagSize)
        {
            if(rand > bagSize)
            {
                rand = bagSize;
            }
            for(int x = 0; x < rand; ++x)
            {
                _items[x] = RandomGenerators.GenerateRandomItem();
                ++_itemCount;
            }
        }

        public Item GetItem(int itemIndex)
        {
            return _items[itemIndex];
            
        }

        public void AddToInventory(Item item)
        {
            _items[_itemCount] = item;
            ++_itemCount;
        }

        public Item RemoveFromInventory(int index)
        {
            Item removedItem = _items[index];
            _items[index] = null;
            for (int x = index; x < _items.Length; ++x)
            {
                if(_items [x + 1] == null)
                {
                    break;
                }
                _items[x] = _items[x + 1];
                _items[x + 1] = null;
            }
            --_itemCount;
            return removedItem;
        }


    }
}
