﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public enum InventorySlotId
    {
        UNEQUIPPABLE = -1,
        HELMET,
        CHESTPIECE,
        GRIEVES,
        VAMBRACES,
        WEAPON,
        POTION1,
    }
}
