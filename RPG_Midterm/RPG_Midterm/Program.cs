﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPG_Midterm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        public static MainMenu form1;
        public static InventoryScreen form2;
        public static CombatScreen form3;
        public static DeathScreen form4;
        //Plays sound on load
        public static System.Media.SoundPlayer player;
        [STAThread]
        
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            form1 = new MainMenu();
            
            player = new System.Media.SoundPlayer(@"C:\Users\micha\Documents\RPG_Midterm\sounds\Kings_Of_Tara.wav");
            /*
            "Kings of Tara" Kevin MacLeod (incompetech.com)
            Licensed under Creative Commons: By Attribution 3.0 License
            http://creativecommons.org/licenses/by/3.0/
            */
            player.Play();
            Application.Run(form1);

        }
    }
}
