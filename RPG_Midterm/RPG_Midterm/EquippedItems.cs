﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class EquippedItems
    {
        private Item[] _slots;
        
        public EquippedItems(params Item[] items)
        {
            _slots = new Item[6];
            if(items.Length <= 6)
            {
                foreach(Item item in items)
                {
                    _slots[(int)item.Slot] = item;
                }
            }
        }
        public Item GetItem(InventorySlotId slot)
        {
            return _slots[(int)slot];
        }
        public Item Equip(Item item)
        {
            Item equippingItem = _slots[(int)item.Slot];
            _slots[(int)item.Slot] = item;
            return equippingItem;
        }
        public Item Unequip(InventorySlotId slot)
        {
            Item unequippingItem = _slots[(int)slot];
            _slots[(int)slot] = null;
            return unequippingItem;
        }
    }
}
