﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public interface IPotion
    {
        int HealValue { get; }
    }
}
