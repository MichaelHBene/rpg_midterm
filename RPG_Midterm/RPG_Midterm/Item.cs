﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class Item
    {
        protected string name;
        protected InventorySlotId slot;
        public string Name
        {
            get { return name; }
        }
        public InventorySlotId Slot
        {
            get { return slot; }
        }

        public override string ToString()
        {
            string displayName = Name;
            Random randSuffix = new Random();
            if (this is IWeapon)
            {
                IWeapon temp = (IWeapon)this;
                int suffixIndex = randSuffix.Next(1, 9);
                string weaponSuffix;
                switch (suffixIndex)
                {
                    case 1:
                        weaponSuffix = " of Badassery";
                        break;
                    case 2:
                        weaponSuffix = " of Complete Annihilation";
                        break;
                    case 3:
                        weaponSuffix = " of Sluggish Atrophy";
                        break;
                    case 4:
                        weaponSuffix = " of Nightmares";
                        break;
                    case 5:
                        weaponSuffix = " of Soul-Rending";
                        break;
                    case 6:
                        weaponSuffix = " of Bloodletting";
                        break;
                    case 7:
                        weaponSuffix = " of Surgical Precision";
                        break;
                    case 8:
                        weaponSuffix = " of War Crimes";
                        break;
                    default:
                        weaponSuffix = "";
                        break;
                   
                }
                displayName += " (+" + temp.AttackValue + ")";
                //enter display name for weapons here
            }
            if (this is IPotion)
            {
                IPotion temp = (IPotion)this;
                int suffixIndex = randSuffix.Next(1, 9);
                string potionSuffix;
                switch (suffixIndex)
                {
                    case 1:
                        potionSuffix = " of Torpor";
                        break;
                    case 2:
                        potionSuffix = " of Rejuvination";
                        break;
                    case 3:
                        potionSuffix = " of Doctoring";
                        break;
                    case 4:
                        potionSuffix = " of Support";
                        break;
                    case 5:
                        potionSuffix = " of Narcotics";
                        break;
                    case 6:
                        potionSuffix = " of Cherry Flavoring";
                        break;
                    case 7:
                        potionSuffix = " of Red Food Dye";
                        break;
                    case 8:
                        potionSuffix = " of Capri Sun";
                        break;
                    default:
                        potionSuffix = "";
                        break;

                }
                displayName += " (+" + temp.HealValue + ")";
                //enter display name for potionshere
            }
            if (this is IArmor)
            {
                IArmor temp = (IArmor)this;
                int suffixIndex = randSuffix.Next(1, 9);
                string armorSuffix;
                switch (suffixIndex)
                {
                    case 1:
                        armorSuffix = " of Bulwark";
                        break;
                    case 2:
                        armorSuffix = " of Pungent Defense";
                        break;
                    case 3:
                        armorSuffix = " of Unyielding AC";
                        break;
                    case 4:
                        armorSuffix = " of Untouchability";
                        break;
                    case 5:
                        armorSuffix = " of Shrouded Decadence";
                        break;
                    case 6:
                        armorSuffix = " of Tankiness";
                        break;
                    case 7:
                        armorSuffix = " of Loincloth.";
                        break;
                    case 8:
                        armorSuffix = " of Heroic Appearance";
                        break;
                    default:
                        armorSuffix = "";
                        break;

                }
                displayName +=" (+" + temp.DefenseValue + ")";
                //enter display name for armor here
            }
            return displayName;
        }

    }
}
