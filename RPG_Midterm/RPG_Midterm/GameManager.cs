﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class GameManager
    {
        private Character _player;
        private Character _enemy;
        private int _depth = 1;
        private bool _gameOver;

        public Character Player { get { return _player; } }
        public Character Enemy { get { return _enemy; } }
        public int Depth { get { return _depth; } }
        public bool IsGameOver { get { return _gameOver; } }

        public GameManager()
        {
            _player = new Player();
            _enemy = RandomGenerators.GenerateRandomEnemy();
        }

        private void ShowGameOver()
        {
            Program.form4.Show();
        }

        public int Attack(Character attacker, Character defender)
        {
            int attackDamage = (attacker.CalcTotalAttackValue() - defender.CalcTotalDefenseValue());
            if(attackDamage < 0)
            {
                attackDamage = 0;
            }
            defender.TakeDamage((attackDamage));
            return attackDamage;
        }

        public int DrinkPotion(Character drinker, IPotion potion)
        {
            return drinker.Heal(potion.HealValue);
        }

        public void ManageInventory()
        {
            Program.form2.Show();
        }

        public void NextBattle()
        {
            if(_depth % 5 == 0)
            {
                _enemy = RandomGenerators.GenerateRandomMiniboss();
                ++_depth;
            }
            else if(_depth % 5 != 0)
            {
                _enemy = RandomGenerators.GenerateRandomEnemy();
                ++_depth;
            }
            
        }
    }
}
