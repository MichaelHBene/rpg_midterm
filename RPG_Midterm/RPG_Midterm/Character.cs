﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RPG_Midterm
{
    public class Character 
    {

        protected StoredItems _bag;
        protected EquippedItems _equipped;
        protected int _maxHealth;
        protected int _currentHealth;
        protected bool _dead;
        protected string _name;
        protected Image _image;
        

        public StoredItems Bag { get { return _bag; } }

        public EquippedItems Equipped { get { return _equipped; } }

        public string Name { get { return _name; } }

        public int MaxHealth { get { return _maxHealth; } }

        public int CurrentHealth { get { return _currentHealth; } }

        public bool IsDead { get { return _dead; } }

        public Image Image { get { return _image; } }

        public Character()
        {
            _maxHealth = 100;
            _currentHealth = _maxHealth;
            _dead = false;
        }

        public int CalcTotalAttackValue()
        {
            int damage = 0;
            for (int x = 0; x < 6; ++x)
            {
                if (_equipped.GetItem((InventorySlotId)x) is IWeapon)
                {
                    IWeapon weapon = (IWeapon)_equipped.GetItem((InventorySlotId)x);
                    damage += weapon.AttackValue;
                }
            }
            return damage;
        }

        public int CalcTotalDefenseValue()
        {
            int defense = 0;
            for (int x = 0; x < 6; ++x)
            {
                if (_equipped.GetItem((InventorySlotId)x) is IArmor)
                {
                    IArmor armor = (IArmor)_equipped.GetItem((InventorySlotId)x);
                    defense += armor.DefenseValue;
                }
            }
            return defense;
        }

        public void TakeDamage(int damage)
        {
            _currentHealth -= damage;
            if (_currentHealth <= 0)
            {
                _dead = true;
                _currentHealth = 0;
            }
        }

        public int Heal(int heal)
        {
            int beforeHealing = CurrentHealth;

            _currentHealth += heal;
            if (_currentHealth > _maxHealth)
            {
                _currentHealth = _maxHealth;
            }

            return CurrentHealth - beforeHealing;
        }
    }
}
