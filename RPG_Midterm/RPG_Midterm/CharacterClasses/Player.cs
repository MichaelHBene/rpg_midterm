﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    public class Player : Character
    {

        public Player()
        {
            _maxHealth = 100;
            _currentHealth = _maxHealth;
            _bag = new StoredItems(20);
            _equipped = new EquippedItems(new BronzeSword(), new Gambeson(), new SmallHealthPotion());
        }
    }
}
