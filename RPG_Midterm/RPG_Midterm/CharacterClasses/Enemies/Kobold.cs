﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Kobold : Enemy
    {
        public Kobold()
        {
            _maxHealth = 50;
            _currentHealth = _maxHealth;
            string generatedName = "";
            _image = RPG_Midterm.Properties.Resources.Kobold;
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);

            Random randomName = new Random();

            int firstSuffix = randomName.Next(1, 5);
            int secondSuffix = randomName.Next(1, 5);
            int nickname = randomName.Next(1, 5);

            switch (firstSuffix)
            {
                case 1:
                    generatedName += "rak";
                    break;
                case 2:
                    generatedName += "yol";
                    break;
                case 3:
                    generatedName += "dar";
                    break;
                case 4:
                    generatedName += "frig";
                    break;
            }
            switch (secondSuffix)
            {
                case 1:
                    generatedName += "dar";
                    break;
                case 2:
                    generatedName += "bob";
                    break;
                case 3:
                    generatedName += "alosh";
                    break;
                case 4:
                    generatedName += "athok";
                    break;
            }
            switch (nickname)
            {
                case 1:
                    generatedName += " Dragon-Kin";
                    break;
                case 2:
                    generatedName += " The Young Wyrm";
                    break;
                case 3:
                    generatedName += " The Wingless";
                    break;
                case 4:
                    generatedName += " The Foul-toothed";
                    break;
            }
            _name = generatedName + " (Kobold)";
            _equipped = new EquippedItems(new BronzeSword(), new Gambeson());
        }
    }
}
