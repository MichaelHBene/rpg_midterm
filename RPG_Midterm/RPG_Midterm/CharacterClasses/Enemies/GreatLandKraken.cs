﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class GreatLandKraken : Enemy
    {
        public GreatLandKraken()
        {
            _maxHealth = 200;
            _currentHealth = _maxHealth;
            _equipped = new EquippedItems(new NaturalWeapon(10, 21), new NaturalArmor(5, 9));
            _image = RPG_Midterm.Properties.Resources.GreatLandKraken;
            string generatedName = "";

            Random randomName = new Random();

            int firstSuffix = randomName.Next(1, 5);
            int secondSuffix = randomName.Next(1, 5);
            int nickname = randomName.Next(1, 5);

            switch (firstSuffix)
            {
                case 1:
                    generatedName += "Gua";
                    break;
                case 2:
                    generatedName += "Bla";
                    break;
                case 3:
                    generatedName += "Tra";
                    break;
                case 4:
                    generatedName += "Fre";
                    break;
            }
            switch (secondSuffix)
            {
                case 1:
                    generatedName += "ba";
                    break;
                case 2:
                    generatedName += "jar";
                    break;
                case 3:
                    generatedName += "dolak";
                    break;
                case 4:
                    generatedName += "bbles";
                    break;
            }
            switch (nickname)
            {
                case 1:
                    generatedName += " The Almighty";
                    break;
                case 2:
                    generatedName += " The All-Seer";
                    break;
                case 3:
                    generatedName += " The Many-Armed";
                    break;
                case 4:
                    generatedName += " Ink-Sprayer";
                    break;
            }
            _name = generatedName + " (Great Land Kraken Miniboss)";
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            Item randomLoot2 = RandomGenerators.GenerateRandomItem();
            Item randomLoot3 = RandomGenerators.GenerateRandomItem();
            Item randomLoot4 = RandomGenerators.GenerateRandomItem();
            Item randomLoot5 = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);
            _bag.AddToInventory(randomLoot2);
            _bag.AddToInventory(randomLoot3);
            _bag.AddToInventory(randomLoot4);
            _bag.AddToInventory(randomLoot5);

        }
    }
}
