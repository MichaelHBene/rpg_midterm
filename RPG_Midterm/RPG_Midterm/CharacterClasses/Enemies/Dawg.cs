﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Dawg : Enemy
    {
        public Dawg()
        {
            _maxHealth = 40;
            _currentHealth = _maxHealth;
            _name = "A Wild Dawg";
            _equipped = new EquippedItems(new NaturalWeapon(2, 5), new NaturalArmor(1, 2));
            _image = RPG_Midterm.Properties.Resources.Dawg;
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);
        }
    }
}
