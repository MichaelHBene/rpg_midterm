﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Blight : Enemy
    {
        public Blight()
        {
            _maxHealth = 120;
            _currentHealth = _maxHealth;
            _equipped = new EquippedItems(new NaturalWeapon(6, 11), new NaturalArmor(4, 7));
            _image = RPG_Midterm.Properties.Resources.Blight;

            string generatedName = "";

            Random randomName = new Random();

            int firstSuffix = randomName.Next(1, 5);
            int secondSuffix = randomName.Next(1, 5);
            int nickname = randomName.Next(1, 5);

            switch (firstSuffix)
            {
                case 1:
                    generatedName += "Yog";
                    break;
                case 2:
                    generatedName += "Cthul";
                    break;
                case 3:
                    generatedName += "Bak";
                    break;
                case 4:
                    generatedName += "Ygg";
                    break;
            }
            switch (secondSuffix)
            {
                case 1:
                    generatedName += "omek";
                    break;
                case 2:
                    generatedName += "solag";
                    break;
                case 3:
                    generatedName += "imar";
                    break;
                case 4:
                    generatedName += "-gol";
                    break;
            }
            switch (nickname)
            {
                case 1:
                    generatedName += " The Quiverring";
                    break;
                case 2:
                    generatedName += " The Slimey";
                    break;
                case 3:
                    generatedName += " The Devourer";
                    break;
                case 4:
                    generatedName += " The Omniscient";
                    break;
            }
            _name = generatedName + " (Blight Boss)";

            Item randomLoot = RandomGenerators.GenerateRandomItem();
            Item randomLoot2 = RandomGenerators.GenerateRandomItem();
            Item randomLoot3 = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);
            _bag.AddToInventory(randomLoot2);
            _bag.AddToInventory(randomLoot3);
        }
    }
}
