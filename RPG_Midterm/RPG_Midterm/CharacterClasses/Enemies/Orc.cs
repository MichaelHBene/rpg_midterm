﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Orc : Enemy
    {
        public Orc()
        {
            _maxHealth = 100;
            _currentHealth = _maxHealth;
            string generatedName = "";
            _image = RPG_Midterm.Properties.Resources.Orc;
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);

            Random randomName = new Random();

            int firstSuffix = randomName.Next(1, 5);
            int secondSuffix = randomName.Next(1, 5);
            int nickname = randomName.Next(1, 5);

            switch (firstSuffix)
            {
                case 1:
                    generatedName += "Grog";
                    break;
                case 2:
                    generatedName += "Klag";
                    break;
                case 3:
                    generatedName += "Borg";
                    break;
                case 4:
                    generatedName += "Grum";
                    break;
            }
            switch (secondSuffix)
            {
                case 1:
                    generatedName += "dash";
                    break;
                case 2:
                    generatedName += "gda";
                    break;
                case 3:
                    generatedName += "mok";
                    break;
                case 4:
                    generatedName += "ish";
                    break;
            }
            switch (nickname)
            {
                case 1:
                    generatedName += " The Carver";
                    break;
                case 2:
                    generatedName += " The Man-eater";
                    break;
                case 3:
                    generatedName += " The Tanner";
                    break;
                case 4:
                    generatedName += " Sharp-tooth";
                    break;
            }
            _name = generatedName + " (Orc)";
            _equipped = new EquippedItems(new IronSword(), new IronChestpiece());
        }
    }
}
