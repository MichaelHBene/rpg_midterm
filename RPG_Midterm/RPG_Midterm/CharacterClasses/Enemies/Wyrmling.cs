﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Wyrmling : Enemy
    {
        public Wyrmling()
        {
            _maxHealth = 150;
            _currentHealth = _maxHealth;
            _equipped = new EquippedItems(new NaturalWeapon(5, 16), new NaturalArmor(5, 11));
            _image = RPG_Midterm.Properties.Resources.Wyrm;
            string generatedName = "";

            Random randomName = new Random();

            int firstSuffix = randomName.Next(1, 5);
            int secondSuffix = randomName.Next(1, 5);
            int nickname = randomName.Next(1, 5);

            switch (firstSuffix)
            {
                case 1:
                    generatedName += "Fesh";
                    break;
                case 2:
                    generatedName += "Bar";
                    break;
                case 3:
                    generatedName += "Grug";
                    break;
                case 4:
                    generatedName += "Smaug";
                    break;
            }
            switch (secondSuffix)
            {
                case 1:
                    generatedName += "dol";
                    break;
                case 2:
                    generatedName += "gas";
                    break;
                case 3:
                    generatedName += "domir";
                    break;
                case 4:
                    generatedName += "fish";
                    break;
            }
            switch (nickname)
            {
                case 1:
                    generatedName += " The Fearsome";
                    break;
                case 2:
                    generatedName += " The Ever-Peckish";
                    break;
                case 3:
                    generatedName += " The Hoarder";
                    break;
                case 4:
                    generatedName += " The Red-Winged";
                    break;
            }
            _name = generatedName + " (Wyrmling Miniboss)";
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            Item randomLoot2 = RandomGenerators.GenerateRandomItem();
            Item randomLoot3 = RandomGenerators.GenerateRandomItem();
            Item randomLoot4 = RandomGenerators.GenerateRandomItem();
            Item randomLoot5 = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);
            _bag.AddToInventory(randomLoot2);
            _bag.AddToInventory(randomLoot3);
            _bag.AddToInventory(randomLoot4);
            _bag.AddToInventory(randomLoot5);
        }
    }
}
