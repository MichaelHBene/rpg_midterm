﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Goblin : Enemy
    {
        public Goblin()
        {
            _maxHealth = 60;
            _currentHealth = _maxHealth;
            string generatedName = "";
            _image = RPG_Midterm.Properties.Resources.Goblin;
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);

            Random randomName = new Random();

            int firstSuffix = randomName.Next(1, 5);
            int secondSuffix = randomName.Next(1, 5);
            int nickname = randomName.Next(1, 5);

            switch (firstSuffix)
            {
                case 1:
                    generatedName += "gub";
                    break;
                case 2:
                    generatedName += "kar";
                    break;
                case 3:
                    generatedName += "gre";
                    break;
                case 4:
                    generatedName += "hig";
                    break;
            }
            switch (secondSuffix)
            {
                case 1:
                    generatedName += "bles";
                    break;
                case 2:
                    generatedName += "gory";
                    break;
                case 3:
                    generatedName += "dal";
                    break;
                case 4:
                    generatedName += "thar";
                    break;
            }
            switch (nickname)
            {
                case 1:
                    generatedName += " Fly-Crusher";
                    break;
                case 2:
                    generatedName += " Dog-Slayer";
                    break;
                case 3:
                    generatedName += " The Rancid";
                    break;
                case 4:
                    generatedName += " The Impotent";
                    break;
            }

            _name = generatedName + " (Goblin)";

            _equipped = new EquippedItems(new IronSword(), new Gambeson());
        }
    }
}
