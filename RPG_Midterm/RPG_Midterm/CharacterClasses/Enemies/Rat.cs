﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Rat : Enemy
    {
        public Rat()
        {
            _maxHealth = 20;
            _currentHealth = _maxHealth;
            _name = "A Large Rat in a Small Suit of Armor";
            _equipped = new EquippedItems(new NaturalWeapon(1, 4), new NaturalArmor(0, 0));
            _image = RPG_Midterm.Properties.Resources.ArmoredRat;
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);
        }
    }
}
