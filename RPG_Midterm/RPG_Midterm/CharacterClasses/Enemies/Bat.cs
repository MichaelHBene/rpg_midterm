﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Midterm
{
    class Bat : Enemy
    {
        public Bat()
        {
            _maxHealth = 30;
            _currentHealth = _maxHealth;
            _name = "A Rampaging Swarm of Bats";
            _equipped = new EquippedItems(new NaturalWeapon(1, 4), new NaturalArmor(3, 4));
            _image = RPG_Midterm.Properties.Resources.BatSwarm;
            Item randomLoot = RandomGenerators.GenerateRandomItem();
            _bag.AddToInventory(randomLoot);
        }
    }
}
